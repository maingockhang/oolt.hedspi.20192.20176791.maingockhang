package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

import hust.soict.ictglobal.aims.media.Media;

public class DigitalVideoDisc extends Disc implements Playable {
  
  private String director;
  private int length;
  public DigitalVideoDisc() {
	super();
}


public boolean search(String title) { // ham tim kiem mot dvd voi title chotruoc 
	String a = title.toLowerCase();
	String b = title.toLowerCase();
	return (b.contains(a));
}
public String getDirector() {
	return director;
}
public void setDirector(String director) {
	this.director = director;
}
public int getLength() {
	return length;
}
public void setLength(int length) {
	this.length = length;
}
@Override
public void play() {
	// TODO Auto-generated method stub
	System.out.println("Playing DVD: "+this.getTitle());
	System.out.println("DVD length:"+this.getLength());
}
}
