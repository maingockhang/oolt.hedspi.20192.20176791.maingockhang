
public class DateUtils {
   public static int compareDates(MyDate s1,MyDate s2) { // tra ve 1 neu s1 < s2 ve 1 neu s1 > s2
	   if(s1.getYear() < s2.getYear()) {
		    return 1;
	   }
	   if(s1.getYear()>s2.getYear()) return -1;
	   else {
		   if(s1.getMonth()<s2.getMonth()) return 1;
		   if(s1.getMonth()>s2.getMonth()) return -1;
		   else {
			   if(s1.getDate()<s2.getDate()) return 1;
			   if(s1.getDate()>s2.getDate()) return -1;
			   else {
				   return 0;
			   }
		   }
	   }
		   
  }
   public static void changePosition(MyDate s1,MyDate s2) {
		int oldDay = s1.getDate();
		int oldM = s1.getMonth();
		int oldY= s1.getYear();
		
		s1.setDate(s2.getDate());
		s1.setMonth(s2.getMonth());
		s1.setYear(s2.getYear());
		
		s2.setDate(oldDay);
		s2.setMonth(oldM);
		s2.setYear(oldY);
	    
	}
   public static void sortDates(MyDate a[]) {
	   for(int i=0; i <a.length;i++) {
		  for(int j=0; j<a.length-i;i++) {
			  if(compareDates(a[j],a[j+1]) == -1) {
				  changePosition(a[j],a[j+1]);
			  }
		  }
	   }
   }
}

