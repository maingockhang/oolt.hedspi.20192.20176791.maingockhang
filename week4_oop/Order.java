
public class Order {
	private int qtyOrdered; //luu so luong mat hang trong don
	public static final int MAX_NUMBER_ORDER=11;
	public static final int MAX_LIMITTED_ORDERS=5;
	private static int nbOrders = 0; // luu so luong don hang 
	private MyDate dateOrderd= new MyDate();
	private DigitalVideoDisc itemsOrdered[]= new DigitalVideoDisc[MAX_NUMBER_ORDER];
	public Order() {
		super();
		if(nbOrders<=MAX_LIMITTED_ORDERS) nbOrders++;
		else {
			System.out.println("You have "+MAX_LIMITTED_ORDERS+",you cant push order");
		}
		this.qtyOrdered=0;
	}
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc){
		if (this.qtyOrdered<MAX_NUMBER_ORDER){
			this.itemsOrdered[qtyOrdered+1]=disc;
			this.qtyOrdered=this.qtyOrdered+1;
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList){
		 for(int i = 0; i < dvdList.length; i++)
		 {
			 if(this.qtyOrdered<MAX_NUMBER_ORDER) {
				 addDigitalVideoDisc(dvdList[i]);
			 }
			 else
			 {
				 System.out.println("So Luong mat hang da vuot qua cho phep");
				 break;
			 }
		 }
	}
	public void printItemOfOrders() {
		System.out.println("***********************Order***********************");
		System.out.print("Date:");
		dateOrderd.print();
		//System.out.println(this.getDateOrderd());
		System.out.println("Ordered Items:");
		for(int i = 1; i<=this.qtyOrdered; i++) {
			System.out.println(i+"-DVD-"+itemsOrdered[i].getTitle()+"-"+itemsOrdered[i].getCategory()+"-"+itemsOrdered[i].getDirector()+"-"+itemsOrdered[i].getLength()+":"+itemsOrdered[i].getCost()+"$"    );
		}
		System.out.println("Total cost:"+this.totalCost());
		System.out.println("***************************************************");
}
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2) {
		
		DigitalVideoDisc dvdList[] = new DigitalVideoDisc[2];
		dvdList[0] = dvd1;
		dvdList[1] = dvd2;
		addDigitalVideoDisc(dvdList);
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc){
		int j=0;
			for (int i=1;i<=this.qtyOrdered;i++){
				if (itemsOrdered[i].getTitle().equals(disc.getTitle())){
					j=1;
					this.qtyOrdered=this.qtyOrdered-1;
				}
				if (j==1){
					itemsOrdered[i]=itemsOrdered[i+1];
				    
				}
			}
	}
	public float totalCost(){
		float tong=0;
		for (int i=1;i<=this.qtyOrdered;i++){
			tong=tong+itemsOrdered[i].getCost();
		}
		return tong;
	
	}

	public MyDate getDateOrderd() {
		return dateOrderd;
	}

	public void setDateOrderd(MyDate dateOrderd) {
		this.dateOrderd = dateOrderd;
	}
	
}
