package hust.soict.ictglobal.aims.order.Date;
import java.util.Calendar;

import javax.swing.JOptionPane;

public class MyDate {
    private int date;
    private int month;
    private int year;
    
	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
    
	public MyDate() {
		Calendar cal = Calendar.getInstance(); 
		setDate(cal.get(Calendar.DAY_OF_MONTH));
		setMonth(cal.get(Calendar.MONTH));
		setYear(cal.get(Calendar.YEAR));
	}
	
	
	
    public MyDate(int date, int month, int year) {
		super();
		this.date = date;
		this.month = month;
		this.year = year;
	}
    
    public void Input() {
    	String strDate,strMonth,strYear;
    	int date,month,year;
    	do {
    		strDate = JOptionPane.showInputDialog(null,"nhap ngay",JOptionPane.INFORMATION_MESSAGE);
    		date = Integer.parseInt(strDate);
    		setDate(date);
    	}while(date < 0 || date > 32);
    	
    	do {
    		strMonth = JOptionPane.showInputDialog(null,"nhap thang",JOptionPane.INFORMATION_MESSAGE);
    		month = Integer.parseInt(strMonth);
    		setMonth(month);
    	}while(month > 12 || month < 0);
    	
    	do {
    		strYear = JOptionPane.showInputDialog(null,"nhap nam",JOptionPane.INFORMATION_MESSAGE );
    		year = Integer.parseInt(strYear);
    		setYear(year);
    	}while(year <0 );
    	
    }
    
   

    public void GetDateFromUser() {
    	String strDate,strMonth,strYear, str;
    	String [] s;
    	int date,month,year;
    	str = JOptionPane.showInputDialog(null,"Input Date",JOptionPane.INFORMATION_MESSAGE);
    	s = str.split(" ");
    	strDate = s[1];
    	strMonth = s[0];
//    	strYear = s[2] +s[3];
    	int s1= convertYear(s[2]);
    	int s2 = convertYear(s[3]);
    	date = convertDate(strDate);setDate(date);
    	month = convertDate(strMonth);setDate(month);
    	year = s1*100 + s2;setYear(year);
    }
    
    public void showDateOnScreen() {
    	System.out.println(this.getDate()+"/"+this.getMonth()+"/"+this.getYear());
    }
    
    public void showDate() {
    	JOptionPane.showMessageDialog(null, "Ngay/Thang/Nam :" + getDate()+ "/" +getMonth()+ "/" +getYear() );
    }
    
    public int convertYear(String s) {
    	int date;
    	String str = s.toLowerCase();
    	switch (str) {
    	case "zero":date =0;break;
    	case "one": date =  1;break;
		case "two": date =  2;break;
        case "three": date = 3;break;
		case "four":  date = 4; break;
		case "five":  date = 5; break;
		case "six":  date = 6; break;
		case "seven":  date = 7; break;
		case "eight":  date = 8; break;
		case "nine":  date = 9; break;
		case "ten":  date = 10; break;
		case "eleven":  date = 11; break;
		case "twelve":  date = 12; break;
		case "thirteen":  date = 13; break;
		case "fourteen":  date = 14; break;
		case "fifteen":  date = 15; break;
		case "sixteen":  date = 16; break;
		case "seventeen":  date = 17; break;
		case "eighteen":  date = 18; break;
		case "nineteen":  date = 19; break;
		case "twenty":  date = 20; break;
		default:
			date = 0;
			break;
		}
		return date;
    }
    public int convertDate(String s) {
    	int date;
    	String str = s.toLowerCase();
    	switch (str) {
		case "first": date =  1;break;
		case "second": date =  2;break;
        case "third": date = 3;break;
		case "forth":  date = 4; break;
		case "fifth":  date = 5; break;
		case "sixth":  date = 6; break;
		case "seventh":  date = 7; break;
		case "eighth":  date = 8; break;
		case "ninth":  date = 9; break;
		case "tenth":  date = 10; break;
		case "eleventh":  date = 11; break;
		case "twelfth":  date = 12; break;
		case "thirteenth":  date = 13; break;
		case "fourteenth":  date = 14; break;
		case "fifteenth":  date = 15; break;
		case "sixteenth":  date = 16; break;
		case "seventeenth":  date = 17; break;
		case "eighteenth":  date = 18; break;
		case "nineteenth":  date = 19; break;
		case "twentieth":  date = 20; break;
		case "twenty-first":  date = 21; break;
		case "twenty-second":  date = 22; break;
		case "twenty-third":  date = 23; break;
		case "twenty-fourth": date = 24;break;
		case "twenty-fifth" : date = 25;break;
		case "twenty-sixth":date = 26;break;
		case "twenty-seventh":date = 27;break;
		case "twenty-eighth": date = 28;break;
		case "twenty-ninth":date = 29;break;
		case "thirtieth":date = 30;break;
		case "thirty-first":date = 31;break;
		case "january": date = 1; break;
		case "february": date = 2; break;
		case "march": date = 3;break;
		case "april":  date = 4; break;
		case "may":  date = 5; break;
		case "june":  date = 6; break;
		case "junly":  date = 7; break;
		case "august":  date = 8; break;
		case "september":  date = 9; break;
		case "october":  date = 10; break;
		case "november":  date = 11; break;
		case "decemver":  date = 12; break;
		default:
			date = 0;
			break;
		}
		return date;
    }
    
    public void print() {
    	System.out.print("Current day is: "+this.getDate()+"/"+this.getMonth()+"/"+this.getYear());
    }
}
