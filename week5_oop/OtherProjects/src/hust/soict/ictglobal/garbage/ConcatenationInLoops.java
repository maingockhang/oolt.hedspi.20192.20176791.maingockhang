package hust.soict.ictglobal.garbage;
import java.io.PrintStream;
import java.util.Random;
public class ConcatenationInLoops {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       Random r = new Random(123);
       long start = System.currentTimeMillis();
       String s ="";
       for(int i=0;i<65536;i++) 
    	   s +=r.nextInt(2);
       
       System.out.println(System.currentTimeMillis()-start);
    	   
    	r = new Random(123);
    	start = System.currentTimeMillis();
    	StringBuilder sp = new StringBuilder();
    	for(int i1=0;i1<65536;i1++) 
    	   sp.append(r.nextInt(2));
    	s=sp.toString();
        
    	
    	System.out.println(System.currentTimeMillis()-start);
       
	}

}
