import java.util.Scanner;

public class PT3 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("nhap a :");
		float a = scanner.nextFloat();
		
		System.out.println("nhap b :");
		float b = scanner.nextFloat();
		
		System.out.println("nhap c :");
		float c = scanner.nextFloat();
		
		float delta = b*b - 4*a*c;
		
		if(delta == 0) System.out.println("pt co nghiem kep : " + -b/(2*a));
		else if(delta < 0) System.out.println("pt vo nghiem");
		else {
			System.out.println("pt co 2 nghiem don ");
			System.out.println("nghiem 1 la : " + (-b + Math.sqrt(delta))/(2*a) );
			System.out.println("nghiem 2 la : " + (-b - Math.sqrt(delta))/(2*a) );
			}
	}
}