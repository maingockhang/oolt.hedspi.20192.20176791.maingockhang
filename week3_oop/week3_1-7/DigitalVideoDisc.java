package week3_1_7;

public class DigitalVideoDisc {
  
  private String title;
  private String category;
  private String director;
  private int leghth;
  private float cost;
public DigitalVideoDisc(String title) {
		super();
		this.title = title;
}
public DigitalVideoDisc(String title, String category) {
	super();
	this.title = title;
	this.category = category;
}
public DigitalVideoDisc(String title, String category, String director) {
	super();
	this.title = title;
	this.category = category;
	this.director = director;
}
public DigitalVideoDisc(String title, String category, String director, int leghth, float cost) {
	super();
	this.title = title;
	this.category = category;
	this.director = director;
	this.leghth = leghth;
	this.cost = cost;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getCategory() {
	return category;
}
public void setCategory(String category) {
	this.category = category;
}
public String getDirector() {
	return director;
}
public void setDirector(String director) {
	this.director = director;
}
public int getLeghth() {
	return leghth;
}
public void setLeghth(int leghth) {
	this.leghth = leghth;
}
public float getCost() {
	return cost;
}
public void setCost(float cost) {
	this.cost = cost;
}
}
