import java.util.Scanner;
public class TaoHinhTamGia{
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter the number of line: ");
        int n = keyboard.nextInt();
        int h =0;
        
        for(int i = 1; i <= n; ++i, h = 0) {
            for(int j = 1; j <= n - i; ++j) {
                System.out.print("  ");
            }

            while(h != 2 * i - 1) {
                System.out.print("* ");
                ++h;
            }

            System.out.println();
        }
    }
}