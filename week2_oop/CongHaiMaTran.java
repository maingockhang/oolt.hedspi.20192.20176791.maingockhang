public class CongHaiMaTran{

    public static void main(String[] args){
        int a[][] = { {1, 3, 5}, {2, 4, 6} };
        int b[][] = { {7, 8, 9}, {10, 12, 12} };
        int c[][] = new int[a.length][a[0].length];

        if(SoSanhKT(a,b) == 0) System.out.println("The size is not equal");
        else {
            addM(c, a, b);
            
            for (int i = 0 ; i < c.length; ++i) {
                for(int j = 0; j < c[i].length; ++j) {
                  System.out.print(c[i][j]);
                  System.out.print("\t");
                }
                
                System.out.print("\n");
              }
        }
    }

    public static int SoSanhKT(int a[][], int b[][]){
        if(a.length!=b.length) return 0;
        else{
            for(int i = 0; i < a.length; i++){
                if(a[i].length!=b[i].length) return 0;
            }
        }

        return 1;
    }

    public static void addM(int c[][], int a[][], int b[][]){
        for(int i = 0; i < a.length; i++){
            for(int j = 0; j < a[i].length; j++){
                c[i][j]= a[i][j] + b[i][j];
            }
        }
    }
}