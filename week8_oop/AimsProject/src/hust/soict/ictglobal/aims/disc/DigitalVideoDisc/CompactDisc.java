package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;


import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Media;

public class CompactDisc extends Disc implements Playable{
	
	private String artist;
	private  ArrayList<Track> tracks  = new ArrayList<Track>();
	
	public String getArtist() {
		return artist;
	}
	
	public void addTrack(Track tr) {
		Track tr2 = tr;
		if(this.tracks.equals(tr2) == true) {
			System.out.println("Track is already in the list of track!");
		}else {
			this.tracks.add(tr2);
		}
	}
	
	public void removeTrack(Track tr) {
		if(tracks.contains(tr)) {
			tracks.remove(tr);
		}else {
			System.out.println("The Track is not exits!");
		}
	}

	public int getLength() {
		int sum = 0;
		for (Track tr : tracks) {
            sum  += tr.getLength();
        }
		return sum;
	}

	@Override
	public void play() {
		for(Track tr : tracks) {
			tr.play();
		}
	}
	
	@Override
	public String toString() {
		return "ID: " + this.getId() +"   CompactDisc [Title: " + this.getTitle() +
				"  -  Category: " + this.getCategory() + "  -  " + "artist: " + artist +
				"  -  tracks: " + tracks + "  -  Cost: " + this.getCost() + "$]";
	}

	public void setTrack(int i) {
		Scanner sc =  new Scanner(System.in);
		System.out.print("Input title track "+ i + ":");
		String s = sc.nextLine();
		
		System.out.print("Input length of track:");
		int len = sc.nextInt();
	    Track  m = new Track(s,len);
	    this.addTrack(m);
	    
	}
	

	public void setTracks(int n ) {
		Scanner sc =  new Scanner(System.in);
		System.out.println("Input name artist: ");
		String s = sc.nextLine();
		this.artist = s;
		for(int i =0;i<n;i++) {
			this.setTrack(i + 1);
		}
	}
	
	@Override
	public int compareTo(Media m) {
		if(this.tracks.size() < ((CompactDisc)m).tracks.size())  return -1;
		else if(this.tracks.size() > ((CompactDisc)m).tracks.size()) return 1;
		else {
			return this.getLength() > ((CompactDisc)m).getLength() ? 1 : this.getLength() < ((CompactDisc)m).getLength() ? -1 : 0;
		}
	}
	
	public CompactDisc() {
		super();
	}

	public CompactDisc(String title, String category, float cost) {
		super(title, category, cost);
	}

	public CompactDisc(String title, String category) {
		super(title, category);
	}

	public CompactDisc(String director) {
		super(director);
	}

}
