package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

import hust.soict.ictglobal.aims.media.Media;

public class DigitalVideoDisc extends Disc implements Playable {
	
	@Override
	public int compareTo(Media m) {
		return this.getCost() > m.getCost() ? 1 : this.getCost() < m.getCost() ? -1 : 0;
	}

	public DigitalVideoDisc() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DigitalVideoDisc(String director) {
		super(director);
		// TODO Auto-generated constructor stub
	}

	public DigitalVideoDisc(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}
	
	public DigitalVideoDisc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}
	
	public DigitalVideoDisc(String title, String category, float cost, String director) {
		super(title, category, cost, director);
		// TODO Auto-generated constructor stub
	}

	public DigitalVideoDisc(String title, String category, float cost, String director, int length) {
		super(title, category, cost, director, length);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void play() {
		if(this.getLength() == 0) {
			System.out.println("You can't play this");
		}else {
			System.out.println("Playing DVD: " +  this.getTitle());
			System.out.println("DVD's length: " + this.getLength());
		}
		
	}

	@Override
	public String toString() {
		return "ID: " + this.getId() +"   DigitalVideoDisc ["+ "Title: " + this.getTitle() +
				"  -  Category: " + this.getCategory() +"  -  Director: " +this.getDirector() +
				"  -  Length: "+ this.getLength() + "  -  Cost: "+ this.getCost() + "$]";
	}
	
}
