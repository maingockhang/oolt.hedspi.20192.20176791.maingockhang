package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

import hust.soict.ictglobal.aims.media.Media;

public class Disc extends Media {

	public Disc() {
		super();
	}
	
	private int length;
	private String director;
	
	public int getLength() {
		return length;
	}
	
	public String getDirector() {
		return director;
	}
	
	public Disc(String title, String category, float cost, String director, int length) {
		super(title, category, cost);
		this.director = director;
		this.length = length;
	}
	
	public Disc(String title, String category, float cost, String director) {
		super(title, category, cost);
		this.director = director;
	}

	public Disc(String title, String category, float cost) {
		super(title, category, cost);
	}

	public Disc(String title, String category) {
		super(title, category);
	}

	public Disc(String title) {
		super(title);
	}


}
