package hust.soict.ictglobal.aims.order.Order;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.Track;
import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.order.Date.MyDate;

//import java.util.Scanner;

public class Order{
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMIT_ORDERS = 5;
	private static int nbOrders = 0;
	private ArrayList<Media> itemsOrdered =  new ArrayList<Media>();
	private MyDate dateOrdered;
	

	public Order () {
		super();
		dateOrdered = new MyDate();
		dateOrdered.GetDateFromUser();
		if(nbOrders <= MAX_LIMIT_ORDERS) Order.setNbOrders(Order.getNbOrders() + 1);
		else System.out.println("You have " + MAX_LIMIT_ORDERS + "orders, so can't order now!");
	}
	
	public static int getNbOrders() {
		return nbOrders;
	}


	public static void setNbOrders(int nbOrders) {
		Order.nbOrders = nbOrders;
	}
	
	public void addMedia(Media m) {
		Media m2 = m;
		if(this.itemsOrdered.size() >= MAX_NUMBERS_ORDERED) {
			System.out.println("The order is full! Can't add " + m2.getTitle());	
		}
		else {
			m2.setId(this.itemsOrdered.size()+1);
			if(this.itemsOrdered.equals(m2) != true) {
				itemsOrdered.add(m2);
			}else {
				System.out.println("Invalid Media!");
			}
		}
	}
	
	public void SortTitle() {
		Collections.sort(this.itemsOrdered);
	}
	
	public void removeMedia(Media m) {
		for(int i = 0; i < this.itemsOrdered.size(); i++) {
			if(itemsOrdered.indexOf(m) == i) {
				itemsOrdered.remove(i);
				System.out.println("Removes " + m.getTitle() + " is successfull!");
				return;	
			}
		}
		System.out.println("not exist!");
	}
	
	public void removeMediaByID(int k) {
		this.removeMedia(itemsOrdered.get(k-1));
	}
	
	public float totalCost() {
		float cost = 0;
		for(int i = 0 ; i < this.itemsOrdered.size(); i ++) {
			cost += itemsOrdered.get(i).getCost();
		}
		return cost;
	}
	
	public void addMediaList(Media [] mList) {
		if(this.itemsOrdered.size() + mList.length <= MAX_NUMBERS_ORDERED) {
			for(int i = 0; i <  mList.length; i ++) {
				this.addMedia(mList[i]);
			}
		}
		else System.out.println("\nThe list's size is over the size of dics, can't add!");
	}
	
	public void addMedia(Media m1, Media m2) {
		this.addMedia(m1);
		this.addMedia(m2);
	}
	
	
	public void showOrder() {
		System.out.println("***************************************************************order***************************************************************************");
		System.out.print("Date: ");
		this.dateOrdered.showDateOnScreen();
		for(Media item: this.itemsOrdered) {
			System.out.println(item);
		}
		System.out.println("\nTotal cost: "+this.totalCost() + '$');
		System.out.println("***********************************************************************************************************************************************\n");
	}
	
}

