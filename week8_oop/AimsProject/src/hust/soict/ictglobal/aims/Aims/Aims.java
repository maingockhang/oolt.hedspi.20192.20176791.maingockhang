package hust.soict.ictglobal.aims.Aims;

import java.util.Scanner;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.CompactDisc;
import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.order.Order.Order;

public class Aims {
	public static void main(String[] args) {
		Order o = null;
		int choiceNumber;
		Scanner sc =  new Scanner(System.in);
			do {
				showMenu();
				choiceNumber = sc.nextInt();
				switch (choiceNumber) {
					case 1:
						o = new Order();
						System.out.println("You have been created a new order! Please add item in order!");
						break;
					case 2:
						if(o == null) {
							System.out.println("You didn't created an order, CREATE now!");
						}else {
							System.out.println("Book(1) or DVD(2) or CD(3)");
							int type = sc.nextInt();
							if(type == 1) {
								Book b = new Book("Nha gia kim", "Triet ly", 50f);
								b.addAuthor("Ngoc Khang");
								o.addMedia(b);
								System.out.println("Add item in the order successfull!");
							}
							else if(type == 2) {
								DigitalVideoDisc dvd = new DigitalVideoDisc("Mai mai", "Action", 100f, "Ngoc Khang", 100);
								DigitalVideoDisc dvd1 = new DigitalVideoDisc("Aba mai ", "Cartoon", 50f, "Ngoc Khang", 100);
								o.addMedia(dvd, dvd1);
								
								System.out.println("Add item in the order successfull! Do you want to play it now (1 to ok)");
								int play = sc.nextInt();
								if(play == 1) {
									dvd.play();
								}
							}
							else if(type == 3) {
								System.out.println("You need adding information of Tracks. How many track you wants to add!");
								CompactDisc comP =  new CompactDisc("CD1", "Bolero", 100f);
								CompactDisc comP1 =  new CompactDisc("CD1", "Bolero", 120f);
								int n = sc.nextInt();
								comP.setTracks(n);
								comP1.setTracks(n);
								o.addMedia(comP, comP1);								
								System.out.println("Add ComPactDisc in the order successfull!");
							}
							else {
								System.out.println("Invalid type!");
							}
						}
						break;
					case 3:
						if(o == null) {
							System.out.println("You didn't created an order, CREATE now!");
						}else {
							System.out.println("Input the id that you want to delete from Order?");
							int id = sc.nextInt();
							o.removeMediaByID(id);
						}
						break;
					case 4:
						if(o == null) {
							System.out.println("You didn't created an order, CREATE now!");
						}else {
							o.SortTitle();
							o.showOrder();
						}
						break;
					case 0:
						System.out.println("See you again");
						System.exit(0);
				}
			}while(choiceNumber != 0);
		
	}
	
	public static void showMenu() {
		System.out.println("\n\nOrder Management Application: ");
		System.out.println("-----------------------------------");
		System.out.println("1.Create order");
		System.out.println("2.Add item to the order");
		System.out.println("3.Delete item by id");
		System.out.println("4.Display the items list of order");
		System.out.println("0.Exit");
		System.out.println("-----------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	

}
