package hust.soict.ictglobal.aims.media;

public abstract class Media implements Comparable<Media> {

	private String title;
	private String category;
    private float cost;
    private int id;
	public Media() {
		
	}
	public int compareTo(Media m) {
		return this.getTitle().compareTo(m.getTitle());
	}
	public Media(String title) {
		this.title = title;
	}
	public Media(String title,String category) {
		this(title);
		this.category=category;
	
	}

	public Media(String title,String category,float cost) {
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	
	

}
