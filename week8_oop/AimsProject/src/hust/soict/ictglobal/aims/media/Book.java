package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.Collections;


import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
public class Book extends Media  {
	private  List<String> authors = new ArrayList<String>();
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrequency = new TreeMap<String, Integer>();
	private String contents;
	public void processContent() {
		StringTokenizer str = new StringTokenizer(contents);
		while(str.hasMoreTokens()) {
			this.contentTokens.add(str.nextToken());
		
	    }
		Collections.sort(this.contentTokens);
        java.util.Iterator<String> iterator = this.contentTokens.iterator();
		
		while(iterator.hasNext())
		{
			String key = iterator.next();
			if(this.wordFrequency.containsKey(key)) // update
			{
				
				int count = this.wordFrequency.get(key) + 1;
				this.wordFrequency.remove(key);
				this.wordFrequency.put(key, count);
			}
			else
			{
				this.wordFrequency.put(key, 1); 
			}
		}
		
    }
	
	public Book() {
		super();
	}
	
	public Book(String title) {
		super(title);
	}
	
	public Book(String title, String category) {
		super(title, category);
	}

	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}
	
	
	
	
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	public void setAuthors(String author)
	{
		this.addAuthor(author);
	}
	public String getContent() {
		return contents;
	}

	public void setContent(String contents) {
		this.contents = contents;
		this.processContent();
	}
    
	@Override
	public String toString() 
	{	
		String tokenList = "";
		String frequencyList = "";
		
		

		java.util.Iterator<String> iterator = this.contentTokens.iterator();
		while(iterator.hasNext())
		{
			tokenList = tokenList + "  " + iterator.next();
		}
		tokenList = tokenList + "\n";
		// show map 
		
		for(Map.Entry<String, Integer> entry: this.wordFrequency.entrySet())
		{
			frequencyList += entry.getKey() + ": " + Integer.toString(entry.getValue()) + "\n";
		}
		
		return "------------------------------------------------\n" +
			   "Title: " + this.getTitle() + "\n" +
			   "Category: " + this.getCategory() + "\n" +
			   "Authors: " + this.authors.toString() + "\n" + 
			   "Cost: " + Float.toString(this.getCost()) + "\n" +
			   "Content: " + this.contents + "\n" +
			   "Length of Contents: " + Integer.toString(this.contentTokens.size()) + "\n" +
			   "The token list: "  + "\n" + 
			   tokenList + 
			   "The word frequency: " + "\n" +
			   frequencyList;
		
	}
	
	public void addAuthor(String nameAuthor) {
		for(String s: this.authors) {
			if(s == nameAuthor) {
				System.out.println(nameAuthor + " is exits!");
			}
		}
		this.authors.add(nameAuthor);
	}
	
	public void removeAuthor(String nameAuthor) {
		for(String s: this.authors) {
			if(s == nameAuthor) {
				this.authors.remove(s);
			}
		}
		System.out.println(nameAuthor + " is not exits!");
	}

	@Override
	public int compareTo(Media o) {
		// TODO Auto-generated method stub
		return 0;
	}	
	
}
