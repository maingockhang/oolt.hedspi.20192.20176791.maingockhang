package hust.soict.ictglobal.aims.media;



import java.util.Scanner;

public class BookTest {
	public static void main(String[] args) {
		Book book = new Book();
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter title: ");
		String title = sc.nextLine();
		System.out.print("Enter category: ");
		String category = sc.nextLine();
		System.out.print("Enter cost: ");
		float cost = Float.parseFloat(sc.nextLine());
		System.out.print("Enter author: ");
		String author = sc.nextLine();
		System.out.print("Enter content: ");
		String content = sc.nextLine();
		
		book.setTitle(title);
		book.setCategory(category);
		book.setCost(cost);
		book.setAuthors(author);
		book.setContent(content);
		
		System.out.println(book);

	}

}
